"""This module contains integration tests.

Note: Run all test on empty database.

1. Run the ``damn_server``::

   uvicorn damn_server.api:app --reload

2. Run the integration tests::

   PYTHONASYNCIODEBUG=1 python3 -m unittest tests/integration.py

"""
import asyncpg
import asynctest
import datetime
import json
import jwt
import requests
from dateutil import parser as dt_parser

from damn_server import conf
from tests import create

DAMN_URL = "http://127.0.0.1:8000"
JWT = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkaXNwbGF5X25hbWUiOiJ0ZXN0dXNlciIsImF2YXRhciI6Imh0dHA6Ly9zb21lLmF2YXRhci51cmwiLCJ0YWdzIjoidW5kIG1laW5lIHRhZ2VuIn0.gTx01I9SK_vErNyFpCOK5q_dBXVwhA24t-Vots-Lzow"

def ep(endpoint=""):
    """Append ``endpoint`` to ``DAMN_URL``."""
    return "{}{}".format(DAMN_URL, endpoint)

class AreaAPI(asynctest.TestCase):
    async def setUp(self):
        conn = await asyncpg.connect("postgresql://{}:{}@{}/{}".format(
            conf.DB_USERNAME,
            conf.DB_PASSWORD,
            conf.DB_HOST,
            conf.DB_NAME,
        ))
        await conn.execute("""

        INSERT INTO users VALUES ('testuser', '{}')
        ON CONFLICT DO NOTHING

        """)
        await conn.close()

    async def tearDown(self):
        conn = await asyncpg.connect("postgresql://{}:{}@{}/{}".format(
            conf.DB_USERNAME,
            conf.DB_PASSWORD,
            conf.DB_HOST,
            conf.DB_NAME,
        ))
        await conn.execute("""

        DELETE FROM current_commits;
        DELETE FROM current_squares;
        DELETE FROM current_areas;
        DELETE FROM users WHERE display_name='testuser';

        """)
        await conn.close()

    async def test_area_create_update(self):
        ta = create.area1().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        assert "aid" in r1
        assert isinstance(r1["aid"], int)
        ta["aid"] = r1["aid"]
        ta["created"] = r1["created"]
        ta["author"] = "testuser"
        del(ta["featurecollection"])
        del(ta["grid"])
        del(r1["squares_to_map"])
        del(r1["squares_to_review"])
        del(r1["squares_done"])
        assert ta == r1
        r = requests.get(ep("/area/{}".format(ta["aid"])))
        assert r.status_code == 200
        r2 = r.json()
        del(r2["squares_to_map"])
        del(r2["squares_to_review"])
        del(r2["squares_done"])
        assert ta == r2

        r = requests.get(ep("/area/{}/commits".format(ta["aid"])))
        assert r.status_code == 200
        comm = r.json()
        last_dt = comm[0]["date"]

        ta["description"]["de"] = "Deutsche Beschreibung."
        r = requests.put(
            ep("/area/{}".format(ta["aid"])),
            json={
                "aid": ta["aid"],
                "description": ta["description"],
            },
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 200
        r3 = r.json()
        del(r3["squares_to_map"])
        del(r3["squares_to_review"])
        del(r3["squares_done"])
        assert ta == r3

        r = requests.get(ep("/area/{}/commits?since={}".format(
            ta["aid"],
            last_dt,
        )))
        assert r.status_code == 200
        comm = r.json()
        for c in comm:
            assert c["date"] > last_dt

        r = requests.get(ep("/area/{}".format(ta["aid"])))
        assert r.status_code == 200
        r4 = r.json()
        del(r4["squares_to_map"])
        del(r4["squares_to_review"])
        del(r4["squares_done"])
        assert ta == r4
        ta2 = create.area2().dict()
        r = requests.post(
            ep("/areas"),
            json=ta2,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r5 = r.json()
        r = requests.get(ep("/areas"))
        r6 = r.json()
        assert len(r6) == 2
        assert r6[0]["aid"] == r1["aid"]
        assert r6[1]["aid"] == r5["aid"]
        r = requests.get(ep("/areas?since={}".format(r1["created"])))
        r7 = r.json()
        assert len(r7) == 1
        assert r7[0]["aid"] == r5["aid"]

    async def test_area_create_with_custom_square_size(self):
        ta = create.area3().dict()
        r = requests.post(
            ep("/areas?ssh=1000&ssw=1000"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        assert "aid" in r1
        assert isinstance(r1["aid"], int)
        ta["aid"] = r1["aid"]
        ta["created"] = r1["created"]
        assert r1["squares_to_map"] == 1
        assert r1["squares_to_review"] == 0
        assert r1["squares_done"] == 0

    async def test_area_nearest_locking_policy(self):
        ta = create.area3b().dict()
        r = requests.post(
            ep("/areas?ssh=1&ssw=0.04"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        assert "aid" in r1
        assert isinstance(r1["aid"], int)
        ta["aid"] = r1["aid"]
        ta["created"] = r1["created"]
        assert r1["squares_to_map"] == 6
        # test mapping
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "map recent"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 6
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 6, "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "map nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 5
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 5, "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "map nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 4
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 4, "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "map nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 3
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 3, "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "map nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 2
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 2, "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "map nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 1
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 1, "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        # test reviewing
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 1
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 1, "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 2
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 2, "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 3
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 3, "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 4
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 4, "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 5
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 5, "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review nearest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 6
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"], 6,)),
            json={"sid": 6, "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201

    async def test_area_newbie_locking_policy(self):
        ta = create.area3b().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        assert "aid" in r1
        assert isinstance(r1["aid"], int)
        ta["aid"] = r1["aid"]
        ta["created"] = r1["created"]
        assert r1["squares_to_map"] == 6
        # test mapping
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "map recent"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.json()["sid"] == 6
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"sid": 6, "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        # test reviewing
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review newbie"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 404
        till = datetime.datetime.utcnow() + datetime.timedelta(days=1)
        r = requests.put(
            ep("/user/testuser"),
            json={"display_name": "testuser", "newbie": str(till)},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 200
        new_token = r.json()["token"]
        ru = jwt.decode(
            new_token,
            conf.JWT_SECRET,
            algorithms=["HS256"],
        )
        assert ru["display_name"] == "testuser"
        assert dt_parser.parse(ru["newbie"], ignoretz=True) == till
        r = requests.post(
            ep("/area/{}/commits".format(ta["aid"])),
            json={"type": "review newbie"},
            headers={"Authorization": "Bearer {}".format(new_token)},
        )
        assert r.status_code == 201

    async def test_area_create_with_custom_square_size(self):
        ta = create.area3a().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        assert "aid" in r1
        assert isinstance(r1["aid"], int)
        ta["aid"] = r1["aid"]
        ta["created"] = r1["created"]
        assert r1["squares_to_map"] == 1
        assert r1["squares_to_review"] == 0
        assert r1["squares_done"] == 0

    async def test_area_bad_create(self):
        ta = create.area1().dict()
        del(ta["description"])
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 422
        r = requests.put(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 405
        r = requests.delete(ep("/areas"), json=ta)
        assert r.status_code == 405
        r = requests.post(
            ep("/area/{}".format(9999)),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 405
        r = requests.delete(ep("/area/{}".format(9999)))
        assert r.status_code == 405

    async def test_area_commits_and_stats(self):
        ta = create.area1().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        ta["aid"] = r1["aid"]
        ta["created"] = r1["created"]
        ta["author"] = "testuser"
        del(ta["featurecollection"])
        del(ta["grid"])
        del(r1["squares_to_map"])
        del(r1["squares_to_review"])
        del(r1["squares_done"])
        assert ta == r1
        r = requests.get(ep("/area/{}/commits".format(ta["aid"])))
        assert r.status_code == 200
        r2 = r.json()
        assert r2[0]["type"] == "to map"
        assert r2[0]["message"] == "Add new square"

    async def test_thinlist(self):
        ta1 = create.area1().dict()
        r = requests.post(
            ep("/areas"),
            json=ta1,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        a1 = r.json()
        ta2 = create.area2().dict()
        r = requests.post(
            ep("/areas"),
            json=ta2,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        a2 = r.json()
        ta3 = create.area3().dict()
        r = requests.post(
            ep("/areas"),
            json=ta3,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        a3 = r.json()
        ta4 = create.area4().dict()
        r = requests.post(
            ep("/areas"),
            json=ta4,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        a4 = r.json()
        r = requests.get(ep("/areas"))
        assert r.status_code == 200
        r1 = r.json()
        assert r1 == [
            {
                "aid": a1["aid"],
                "tags": ta1["tags"],
                "priority": ta1["priority"],
                "created": a1["created"],
                "author": "testuser",
                "squares_to_map": 4,
                "squares_to_review": 0,
                "squares_done": 0,
            },
            {
                "aid": a2["aid"],
                "tags": ta2["tags"],
                "priority": ta2["priority"],
                "created": a2["created"],
                "author": "testuser",
                "squares_to_map": 1,
                "squares_to_review": 0,
                "squares_done": 0,
            },
            {
                "aid": a3["aid"],
                "tags": ta3["tags"],
                "priority": ta3["priority"],
                "created": a3["created"],
                "author": "testuser",
                "squares_to_map": 468,
                "squares_to_review": 0,
                "squares_done": 0,
            },
            {
                "aid": a4["aid"],
                "tags": ta4["tags"],
                "priority": ta4["priority"],
                "created": a4["created"],
                "author": "testuser",
                "squares_to_map": 783,
                "squares_to_review": 0,
                "squares_done": 0,
            },
        ]

    async def test_area_map_and_review(self):
        ta = create.area1().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "map oldest"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r2 = r.json()
        assert "aid" in r2
        assert r2["aid"] == r1["aid"]
        assert "sid" in r2
        assert r2["sid"] == 1
        r = requests.get(ep("/area/{}/commits".format(r1["aid"])))
        assert r.status_code == 200
        r3 = r.json()
        assert r3[0]["aid"] == r2["aid"]
        assert r3[0]["sid"] == r2["sid"]
        assert r3[0]["type"] == "locked"
        r = requests.post(
            ep("/area/{}/commits".format(r2["aid"])),
            json={"sid": r2["sid"], "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        # test review
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "review recent"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r5 = r.json()
        assert "aid" in r5
        assert r5["aid"] == r2["aid"]
        assert "sid" in r5
        r = requests.get(ep("/area/{}/commits".format(r2["aid"])))
        assert r.status_code == 200
        r6 = r.json()
        assert r6[0]["aid"] == r5["aid"]
        assert r6[0]["sid"] == r5["sid"]
        assert r6[0]["type"] == "locked"
        r = requests.post(
            ep("/area/{}/commits".format(r5["aid"])),
            json={"sid": r5["sid"], "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.get(ep("/area/{}/commits".format(r5["aid"])))
        assert r.status_code == 200
        r8 = r.json()
        r8 = tuple([c for c in r8 if c["sid"] == r5["sid"]])
        commit_types = ["to map", "locked", "to review", "locked", "done"]
        commit_types.reverse()
        should_types = [c["type"] for c in r8]
        assert commit_types == should_types
        # map all the remaining squares
        for i in range(3):
            r = requests.post(
                ep("/area/{}/commits".format(r1["aid"])),
                json={"type": "map random"},
                headers={"Authorization": "Bearer {}".format(JWT)},
            )
            r = requests.post(
                ep("/area/{}/commits".format(r.json()["aid"])),
                json={"sid": r.json()["sid"], "type": "needs review"},
                headers={"Authorization": "Bearer {}".format(JWT)},
            )
        # review all the remaining squares
        for i in range(3):
            r = requests.post(
                ep("/area/{}/commits".format(r1["aid"])),
                json={"type": "review random"},
                headers={"Authorization": "Bearer {}".format(JWT)},
            )
            r = requests.post(
                ep("/area/{}/commits".format(r.json()["aid"])),
                json={"sid": r.json()["sid"], "type": "is done"},
                headers={"Authorization": "Bearer {}".format(JWT)},
            )
        # map request failed
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "map random"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 404
        assert r.json()["detail"] == "no square to map random"
        # review request failed
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "review random"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 404
        assert r.json()["detail"] == "no square to review random"

    async def test_user_commits(self):
        ta = create.area1().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "map recent"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r2 = r.json()
        assert r2["aid"] == r1["aid"]
        r = requests.post(
            ep("/area/{}/commits".format(r2["aid"])),
            json={"sid": r2["sid"], "type": "needs review"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "review recent"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r3 = r.json()
        assert r3["aid"] == r1["aid"]
        r = requests.post(
            ep("/area/{}/commits".format(r3["aid"])),
            json={"sid": r3["sid"], "type": "is done"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "map recent"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r4 = r.json()
        assert r4["aid"] == r1["aid"]
        r = requests.post(
            ep("/area/{}/commits".format(r4["aid"])),
            json={"sid": r4["sid"], "type": "needs mapping"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.get(ep("/user/testuser/commits"))
        assert r.status_code == 200
        r5 = r.json()
        assert len(r5) == 10
        for c in r5:
            assert c["author"] == "testuser"
        assert r5[0]["sid"] == 3
        assert r5[0]["type"] == "to map"
        assert r5[1]["sid"] == 3
        assert r5[1]["type"] == "locked"
        assert r5[2]["sid"] == 4
        assert r5[2]["type"] == "done"
        assert r5[3]["sid"] == 4
        assert r5[3]["type"] == "locked"
        assert r5[4]["sid"] == 4
        assert r5[4]["type"] == "to review"
        assert r5[5]["sid"] == 4
        assert r5[5]["type"] == "locked"
        assert r5[6]["sid"] == 4
        assert r5[6]["type"] == "to map"
        assert r5[7]["sid"] == 3
        assert r5[7]["type"] == "to map"
        assert r5[8]["sid"] == 2
        assert r5[8]["type"] == "to map"
        assert r5[9]["sid"] == 1
        assert r5[9]["type"] == "to map"

    async def test_square_split(self):
        ta = create.area2().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"type": "map recent"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r2 = r.json()
        aid = r2["aid"]
        sid = r2["sid"]
        r = requests.get(ep("/area/{}/commits".format(aid)))
        assert r.status_code == 200
        comm = r.json()
        comm = tuple([c for c in comm if c["sid"] == sid])
        last_dt = comm[0]["date"]
        r = requests.post(
            ep("/area/{}/commits".format(r2["aid"])),
            json={"sid": r2["sid"], "type": "split"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r = requests.get(ep("/area/{}/commits?since={}".format(aid, last_dt)))
        assert r.status_code == 200
        comm = r.json()
        comm = tuple([c for c in comm if c["sid"] == sid])
        for c in comm:
            assert c["date"] > last_dt

    async def test_square_lock(self):
        ta = create.area2().dict()
        r = requests.post(
            ep("/areas"),
            json=ta,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r1 = r.json()
        r = requests.post(
            ep("/area/{}/commits".format(r1["aid"])),
            json={"sid": 1, "type": "lock"},
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 201
        r2 = r.json()
        assert r2["aid"] == r1["aid"]
        assert r2["sid"] == 1
        r = requests.get(ep("/area/{}/commits".format(r2["aid"])))
        assert r.status_code == 200
        commits = r.json()
        assert commits[0]["aid"] == r2["aid"]
        assert commits[0]["sid"] == r2["sid"]
        assert commits[0]["author"] == "testuser"
        assert commits[0]["type"] == "locked"

    async def test_update_user(self):
        nu = {
            "display_name": "testuser",
            "avatar": "http://another.avatar.url",
            "tags": "my tags",
        }
        r = requests.put(
            ep("/user/testuser"),
            json=nu,
            headers={"Authorization": "Bearer {}".format(JWT)},
        )
        assert r.status_code == 200
        du = jwt.decode(
            r.json()["token"],
            conf.JWT_SECRET,
            algorithms=['HS256'],
        )
        assert du["display_name"] == nu["display_name"]
        assert du["avatar"] == nu["avatar"]
        assert du["tags"] == nu["tags"]
