"""Test area procedures."""
import asynctest
import json
from pydantic import ValidationError

import create

from damn_server import db
from damn_server import area
from damn_server.area import Area
from damn_server.area import UpdateArea
from damn_server import list_of
from damn_server import new
from damn_server.new import CreateCommit

class AreaSave(asynctest.TestCase):
    async def setUp(self):
        await db.init_pool()
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                INSERT INTO users VALUES ('testuser', '{}')
                ON CONFLICT DO NOTHING

                """)
        # Based on data in the database, what is next aid?
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT last_value FROM current_areas_aid_seq

                """)
                self.next_aid = r[0][0]
                if self.next_aid == 9999:
                    self.next_aid = 1000
                else:
                    self.next_aid += 1

    async def tearDown(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                DELETE FROM current_commits;
                DELETE FROM current_squares;
                DELETE FROM current_areas;
                DELETE FROM users WHERE display_name='testuser';

                """)
        await db.pool.close()
        db.pool = None

    async def test_new_area_save_load(self):
        # Create and save test area.
        test_area = create.area1()
        new_aid = await area.save(test_area, "testuser")
        assert self.next_aid == new_aid
        # Load the same area.
        la = await area.load(self.next_aid)
        test_area = Area(
            aid=new_aid,
            tags=test_area.tags,
            priority=test_area.priority,
            description=test_area.description,
            instructions=test_area.instructions,
            created=la.created,
            author="testuser",
            squares_to_map=4,
            squares_to_review=0,
            squares_done=0,
        )
        assert test_area == la
        # Test squares of test area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_squares WHERE aid={aid}
                ORDER BY sid ASC

                """.format(aid=self.next_aid))
                assert len(r) == 4
        i = 1
        for row in r:
            assert row["aid"] == self.next_aid
            assert row["sid"] == i
            i += 1
        # Test commits of test area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_commits WHERE aid={aid}

                """.format(aid=self.next_aid))
                assert len(r) == 4
        for row in r:
            assert row["aid"] == self.next_aid
            assert row["author"] == "testuser"
            assert row["type"] == "to map"
            assert row["message"] == "Add new square"

    async def test_new_area_save_load_with_square_size(self):
        # Create and save test area.
        test_area = create.area3a()
        new_aid = await area.save(test_area, "testuser")
        assert self.next_aid == new_aid
        # Load the same area.
        la = await area.load(self.next_aid)
        test_area = Area(
            aid=new_aid,
            tags=test_area.tags,
            priority=test_area.priority,
            description=test_area.description,
            instructions=test_area.instructions,
            created=la.created,
            author="testuser",
            squares_to_map=1,
            squares_to_review=0,
            squares_done=0,
        )
        assert test_area == la
        # Test squares of test area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_squares WHERE aid={aid}

                """.format(aid=self.next_aid))
                assert len(r) == 1
        assert r[0]["aid"] == self.next_aid
        assert r[0]["sid"] == 1
        # Test commits of test_area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_commits WHERE aid={aid}

                """.format(aid=self.next_aid))
                assert len(r) == 1
        assert r[0]["aid"] == self.next_aid
        assert r[0]["author"] == "testuser"
        assert r[0]["type"] == "to map"
        assert r[0]["message"] == "Add new square"

    async def test_area_split_not_overlapping_circles(self):
        # Create and save test_area.
        test_area = create.area3()
        new_aid = await area.save(test_area, "testuser")
        assert self.next_aid == new_aid
        # Load same_area.
        la = await area.load(self.next_aid)
        test_area = Area(
            aid=new_aid,
            tags=test_area.tags,
            priority=test_area.priority,
            description=test_area.description,
            instructions=test_area.instructions,
            created=la.created,
            author="testuser",
            squares_to_map=468,
            squares_to_review=0,
            squares_done=0,
        )
        assert test_area == la
        # Test squares of test_area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_squares WHERE aid={aid} ORDER by sid

                """.format(aid=self.next_aid))
                assert len(r) == test_area.squares_to_map
        i = 1
        for row in r:
            assert row["aid"] == self.next_aid
            assert row["sid"] == i
            i += 1
        # Test commits of test_area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_commits WHERE aid={aid}

                """.format(aid=self.next_aid))
                assert len(r) == test_area.squares_to_map
        for row in r:
            assert row["aid"] == self.next_aid
            assert row["author"] == "testuser"
            assert row["type"] == "to map"
            assert row["message"] == "Add new square"

    async def test_area_split_overlapping_circles(self):
        # Create and save test_area.
        test_area = create.area4()
        new_aid = await area.save(test_area, "testuser")
        assert self.next_aid == new_aid
        # Load same_area.
        la = await area.load(self.next_aid)
        test_area = Area(
            aid=new_aid,
            tags=test_area.tags,
            priority=test_area.priority,
            description=test_area.description,
            instructions=test_area.instructions,
            created=la.created,
            author="testuser",
            squares_to_map=783,
            squares_to_review=0,
            squares_done=0,
        )
        assert test_area == la
        # Test squares of test_area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_squares WHERE aid={aid} ORDER by sid

                """.format(aid=self.next_aid))
                assert len(r) == test_area.squares_to_map
        i = 1
        for row in r:
            assert row["aid"] == self.next_aid
            assert row["sid"] == i
            i += 1
        # Test commits of test_area.
        async with db.pool.acquire() as con:
            async with con.transaction():
                r = await con.fetch("""

                SELECT * FROM current_commits WHERE aid={aid}

                """.format(aid=self.next_aid))
                assert len(r) == test_area.squares_to_map
        for row in r:
            assert row["aid"] == self.next_aid
            assert row["author"] == "testuser"
            assert row["type"] == "to map"
            assert row["message"] == "Add new square"

class AreaProcedures(asynctest.TestCase):
    async def setUp(self):
        await db.init_pool()
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                INSERT INTO users VALUES ('testuser', '{}')
                ON CONFLICT DO NOTHING

                """)
        self.aid1 = await area.save(create.area1(), "testuser")
        self.aid2 = await area.save(create.area2(), "testuser")
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am reviewing"),
                   (2, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (3, self.aid1, "testuser", "locked", "I am mapping"),
                   (3, self.aid1, "testuser", "to review", "Check it out"),
                   (3, self.aid1, "testuser", "locked", "I am reviewing"),
                   (3, self.aid1, "testuser", "done", "Square done"),
                ])

    async def tearDown(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                DELETE FROM current_commits;
                DELETE FROM current_squares;
                DELETE FROM current_areas;
                DELETE FROM users WHERE display_name='testuser';

                """)
        await db.pool.close()
        db.pool = None

    async def test_area_commits(self):
        commits = await list_of.area_commits(self.aid1)
        assert isinstance(commits, tuple)
        assert len(commits) == 14
        sids = (3, 3, 3, 3, 4, 4, 2, 2, 2, 2, 4, 3, 2, 1)
        i = 0
        for c in commits:
            assert c.aid == self.aid1
            assert c.sid == sids[i]
            i += 1
            assert c.author == "testuser"

    async def test_area_update(self):
        ta = await area.load(self.aid1)
        # Update tags.
        ua = UpdateArea(aid=self.aid1, tags="#new #list")
        await area.update(ua, "testuser")
        na = await area.load(self.aid1)
        assert na.tags == "#new #list"
        comm = await list_of.area_commits(self.aid1)
        assert comm[0].sid is None
        assert comm[0].type == "update"
        la = json.loads(comm[0].message)
        la["aid"] = self.aid1
        la["created"] = ta.created
        la["author"] = ta.author
        la["squares_to_map"] = ta.squares_to_map
        la["squares_to_review"] = ta.squares_to_review
        la["squares_done"] = ta.squares_done
        la = Area(**la)
        assert la == ta
        # Update priority and description.
        d = dict(ta.description)
        d["es"] = "descripción en español"
        ua = UpdateArea(
            aid=self.aid1,
            priority=9,
            description=d,
        )
        await area.update(ua, "testuser")
        na = await area.load(self.aid1)
        assert na.priority == 9
        assert na.description == d
        comm = await list_of.area_commits(self.aid1)
        assert comm[0].sid is None
        assert comm[0].type == "update"
        assert json.loads(comm[0].message)["priority"] == 10
        assert json.loads(comm[0].message)["description"] == ta.description
        # Update instructions.
        ua = UpdateArea(
            aid=self.aid1,
            instructions={"buildings": "another wiki"},
        )
        await area.update(ua, "testuser")
        na = await area.load(self.aid1)
        assert na.instructions == {"buildings": "another wiki"}
        comm = await list_of.area_commits(self.aid1)
        assert comm[0].sid is None
        assert comm[0].type == "update"
        assert json.loads(comm[0].message)["instructions"] == {
            "buildings": "another wiki",
            "roads": "https://wiki.openstreetmap.org/wiki/Highway_Tag_Africa",
        }

    async def test_area_map(self):
        c = CreateCommit(type="map recent")
        s = await new.commit(c, self.aid1, "testuser")
        assert s.sid == 4
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == s.sid
        assert commits[0].type == "locked"
        c = CreateCommit(type="map recent")
        s = await new.commit(c, self.aid1, "testuser")
        assert s.sid == 4
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == s.sid
        assert commits[0].type == "locked"

    async def test_area_map_random(self):
        c = CreateCommit(type="map random")
        s = await new.commit(c, self.aid1, "testuser")
        sid1 = s.sid
        assert sid1 in [1, 2, 4]
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == sid1
        assert commits[0].type == "locked"
        s = await new.commit(c, self.aid1, "testuser")
        sid2 = s.sid
        assert sid2 in [1, 2, 4]
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == sid2
        assert commits[0].type == "locked"
        s = await new.commit(c, self.aid1, "testuser")
        sid3 = s.sid
        assert sid3 in [1, 2, 4]
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == sid3
        assert commits[0].type == "locked"
        s = await new.commit(c, self.aid1, "testuser")
        sid4 = s.sid
        assert sid4 in [1, 2, 4]
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == sid4
        assert commits[0].type == "locked"

    async def test_area_review(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                ])
        c = CreateCommit(type="review recent")
        s = await new.commit(c, self.aid1, "testuser")
        assert s.sid == 2
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == s.sid
        assert commits[0].type == "locked"
        s = await new.commit(c, self.aid1, "testuser")
        assert s.sid == 2
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == s.sid
        assert commits[0].type == "locked"

    async def test_area_review_random(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                ])
        c = CreateCommit(type="review random")
        s = await new.commit(c, self.aid1, "testuser")
        sid1 = s.sid
        assert sid1 in [1, 2, 4]
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == sid1
        assert commits[0].type == "locked"
        s = await new.commit(c, self.aid1, "testuser")
        sid2 = s.sid
        assert sid2 in [1, 2, 4]
        commits = await list_of.area_commits(self.aid1)
        assert commits[0].aid == self.aid1
        assert commits[0].sid == sid2
        assert commits[0].type == "locked"

    async def test_area_commits_since(self):
        ta = await area.load(self.aid1)
        await area.update(
            UpdateArea(aid=self.aid1, tags="#new #list"),
            "testuser",
        )
        comm = await list_of.area_commits(self.aid1, ta.created)
        for c in comm:
            assert c.date > ta.created
