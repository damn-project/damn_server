"""Test areas procedures."""
import asynctest
import datetime
import json

import create

from damn_server import db
from damn_server import area
from damn_server import list_of
from damn_server.list_of import ThinArea

class ListOfWhateverProcedures(asynctest.TestCase):
    async def setUp(self):
        await db.init_pool()
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                INSERT INTO users VALUES ('testuser', '{}')
                ON CONFLICT DO NOTHING

                """)
        self.aid1 = await area.save(create.area1(), "testuser")
        self.aid2 = await area.save(create.area2(), "testuser")
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am reviewing"),
                   (2, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (3, self.aid1, "testuser", "locked", "I am mapping"),
                   (3, self.aid1, "testuser", "to review", "Check it out"),
                   (3, self.aid1, "testuser", "locked", "I am reviewing"),
                   (3, self.aid1, "testuser", "done", "Square done"),
                ])
        # Update areas statistics
        # see ``damndb`` of the ``damn_deploy``
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                WITH areas AS (
                    SELECT DISTINCT ON (sid, aid) aid, sid, type
                    FROM current_commits
                    WHERE (
                        type='to map'
                        OR type='to review'
                        OR type='done'
                    )
                    ORDER BY aid, sid, cid DESC
                ),
                to_map AS (
                    SELECT aid, count(sid) as cnt
                    FROM areas
                    WHERE type='to map'
                    GROUP BY aid
                ),
                to_review AS (
                    SELECT aid, count(sid) as cnt
                    FROM areas
                    WHERE type='to review'
                    GROUP BY aid
                ),
                done AS (
                    SELECT aid, count(sid) as cnt
                    FROM areas
                    WHERE type='done'
                    GROUP BY aid
                )
                UPDATE
                    current_areas AS ca
                SET
                    squares_to_map=coalesce(
                    (SELECT cnt FROM to_map WHERE to_map.aid=ca.aid),0),

                    squares_to_review=coalesce(
                    (SELECT cnt FROM to_review WHERE to_review.aid=ca.aid),0),

                    squares_done=coalesce(
                    (SELECT cnt FROM done WHERE done.aid=ca.aid),0)
                ;

                """)

    async def tearDown(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                DELETE FROM current_commits;
                DELETE FROM current_squares;
                DELETE FROM current_areas;
                DELETE FROM users WHERE display_name='testuser';

                """)
        await db.pool.close()
        db.pool = None

    async def test_list_of_thin_areas(self):
        tl = await list_of.thin_areas()
        assert isinstance(tl, tuple)
        assert len(tl) == 2
        for a in tl:
            assert isinstance(a, ThinArea)
        # Check the 1st test area.
        assert tl[0].aid == self.aid1
        assert tl[0].tags == "#list #of #tags"
        assert tl[0].priority == 10
        assert tl[0].squares_to_map == 3
        assert tl[0].squares_to_review == 0
        assert tl[0].squares_done == 1
        # Check the 2nd test area.
        assert tl[1].aid == self.aid2
        assert tl[1].tags == "#some #other #hohoho"
        assert tl[1].priority == 5
        assert tl[1].squares_to_map == 1
        assert tl[1].squares_to_review == 0
        assert tl[1].squares_done == 0

    async def test_list_of_thin_areas_since(self):
        since = datetime.datetime.utcnow()
        aid3 = await area.save(create.area3(), "testuser")
        tl = await list_of.thin_areas(since)
        assert len(tl) == 1
        assert tl[0].aid == aid3
