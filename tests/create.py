"""This module creates test areas."""
import json

from damn_server.area import AreaGrid
from damn_server.area import CreateArea

def area1() -> CreateArea:
    """Create testing area with all information."""
    with open("tests/data/squares.geojson") as f:
        geojson_squares = json.loads(f.read())
    return CreateArea(
        tags="#list #of #tags",
        priority=10,
        description={
            "en": "English description.",
            "cs": "Český popis.",
        },
        instructions={
            "buildings": "another wiki",
            "roads": "https://wiki.openstreetmap.org/wiki/Highway_Tag_Africa",
        },
        featurecollection=geojson_squares,
    )

def area2() -> CreateArea:
    """Create testing area no. 2 with all information."""
    with open("tests/data/square.geojson") as f:
        geojson_square = json.loads(f.read())
    return CreateArea(
        tags="#some #other #hohoho",
        priority=5,
        description={
            "en": "Next example description."
        },
        instructions={
            "buildings": "no wiki yet",
        },
        featurecollection=geojson_square,
    )

def area3() -> CreateArea:
    """Create testing area no. 3 with all information."""
    with open("tests/data/circles1.geojson") as f:
        geojson_circles = json.loads(f.read())
    return CreateArea(
        tags="#some #tags #hohoho",
        priority=1,
        description={
            "en": "Example description."
        },
        instructions={
            "buildings": "no wiki yet",
        },
        featurecollection=geojson_circles,
    )

def area3a() -> CreateArea:
    """Create testing area no. 3 with all information and grid."""
    with open("tests/data/circles1.geojson") as f:
        geojson_circles = json.loads(f.read())
    return CreateArea(
        tags="#some #tags #hohoho",
        priority=1,
        description={
            "en": "Example description."
        },
        instructions={
            "buildings": "no wiki yet",
        },
        featurecollection=geojson_circles,
        grid=AreaGrid(lat=1000, lon=1000)
    )

def area3b() -> CreateArea:
    """Create testing area no. 3 with all information and grid."""
    with open("tests/data/circles1.geojson") as f:
        geojson_circles = json.loads(f.read())
    return CreateArea(
        tags="#some #tags #hohoho",
        priority=1,
        description={
            "en": "Example description."
        },
        instructions={
            "buildings": "no wiki yet",
        },
        featurecollection=geojson_circles,
        grid=AreaGrid(lat=1, lon=0.04)
    )

def area4() -> CreateArea:
    """Create testing area no. 3 with all information."""
    with open("tests/data/circles2.geojson") as f:
        geojson_circles = json.loads(f.read())
    return CreateArea(
        tags="#some #tags #hohoho",
        priority=1,
        description={
            "en": "Example description."
        },
        instructions={
            "buildings": "no wiki yet",
        },
        featurecollection=geojson_circles,
    )
