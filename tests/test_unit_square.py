"""Test square procedures."""
import asynctest
import json

import create

from damn_server import db
from damn_server import area
from damn_server import list_of
from damn_server import new
from damn_server.new import CreateCommit
from damn_server import square

class SquareProcedures(asynctest.TestCase):
    async def setUp(self):
        await db.init_pool()
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                INSERT INTO users VALUES ('testuser', '{}')
                ON CONFLICT DO NOTHING

                """)
        self.aid1 = await area.save(create.area1(), "testuser")
        self.aid2 = await area.save(create.area2(), "testuser")
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am reviewing"),
                   (2, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (3, self.aid1, "testuser", "locked", "I am mapping"),
                   (3, self.aid1, "testuser", "to review", "Check it out"),
                   (3, self.aid1, "testuser", "locked", "I am reviewing"),
                   (3, self.aid1, "testuser", "done", "Square done"),
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                ])

    async def tearDown(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                DELETE FROM current_commits;
                DELETE FROM current_squares;
                DELETE FROM current_areas;

                """)
                await con.execute("""

                DELETE FROM users WHERE display_name='testuser';

                """)
        await db.pool.close()
        db.pool = None

    async def test_square_split(self):
        a1b1 = await square.geojson_border(self.aid1, 1)
        assert "type" in a1b1
        assert "coordinates" in a1b1
        assert a1b1["type"] == "Polygon"
        assert a1b1["coordinates"] == [[
            [30.28,-3.68],
            [30.29,-3.68],
            [30.29,-3.67],
            [30.28,-3.67],
            [30.28,-3.68],
        ]]
        a1b2 = await square.geojson_border(self.aid1, 2)
        assert "type" in a1b2
        assert "coordinates" in a1b2
        assert a1b2["type"] == "Polygon"
        assert a1b2["coordinates"] == [[
            [30.27,-3.67],
            [30.28,-3.67],
            [30.28,-3.66],
            [30.27,-3.66],
            [30.27,-3.67],
        ]]
        a1b3 = await square.geojson_border(self.aid1, 3)
        assert "type" in a1b3
        assert "coordinates" in a1b3
        assert a1b3["type"] == "Polygon"
        assert a1b3["coordinates"] == [[
            [30.27,-3.68],
            [30.28,-3.68],
            [30.28,-3.67],
            [30.27,-3.67],
            [30.27,-3.68],
        ]]
        a1b4 = await square.geojson_border(self.aid1, 4)
        assert "type" in a1b4
        assert "coordinates" in a1b4
        assert a1b4["type"] == "Polygon"
        assert a1b4["coordinates"] == [[
            [30.28,-3.67],
            [30.29,-3.67],
            [30.29,-3.66],
            [30.28,-3.66],
            [30.28,-3.67],
        ]]
        a2b0 = await square.geojson_border(self.aid2, 1)
        assert "type" in a2b0
        assert "coordinates" in a2b0
        assert a2b0["type"] == "Polygon"
        assert a2b0["coordinates"] == [[
            [30.27,-3.66],
            [30.29,-3.66],
            [30.29,-3.68],
            [30.27,-3.68],
            [30.27,-3.66],
        ]]
        commits = await list_of.user_commits("testuser")
        since = commits[0].date
        c = CreateCommit(sid=1, type="lock")
        r = await new.commit(c, self.aid2, "testuser")
        c = CreateCommit(sid=1, type="split")
        r = await new.commit(c, self.aid2, "testuser")
        commits = await list_of.user_commits("testuser", since)
        commits = tuple([
            {
                "sid": c.sid,
                "author": c.author,
                "type": c.type,
                "message": c.message,
            } for c in commits
        ])
        assert commits == (
            {
                "sid": 1,
                "author": "testuser",
                "type": "splitted",
                "message": "",
            },
            {
                "sid": 5,
                "author": "testuser",
                "type": "to map",
                "message": "Sub-square of 1",
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "to map",
                "message": "Sub-square of 1",
            },
            {
                "sid": 3,
                "author": "testuser",
                "type": "to map",
                "message": "Sub-square of 1",
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "to map",
                "message": "Sub-square of 1",
            },
            {
                "sid": 1,
                "author": "testuser",
                "type": "locked",
                "message": "",
            },
        )
        commits = await list_of.area_commits(self.aid2)
        commits = tuple([
            {
                "sid": c.sid,
                "author": c.author,
                "type": c.type,
                "message": c.message,
            } for c in commits if c.sid == 1
        ])
        assert len(commits) == 3
        commits[-1]["aid"] = self.aid2
        commits[-1]["sid"] = 1
        commits[-1]["type"] = "done"
        commits[-1]["message"] = "The square was splitted"
        a2b1 = await square.geojson_border(self.aid2, 2)
        assert "type" in a2b1
        assert "coordinates" in a2b1
        assert a2b1["type"] == "Polygon"
        a2b2 = await square.geojson_border(self.aid2, 3)
        assert "type" in a2b2
        assert "coordinates" in a2b2
        assert a2b2["type"] == "Polygon"
        a2b3 = await square.geojson_border(self.aid2, 4)
        assert "type" in a2b3
        assert "coordinates" in a2b3
        assert a2b3["type"] == "Polygon"
        a2b4 = await square.geojson_border(self.aid2, 5)
        assert "type" in a2b4
        assert "coordinates" in a2b4
        assert a2b4["type"] == "Polygon"
        all_the_squares_borders = (
            a2b1["coordinates"][0]
            + a2b2["coordinates"][0]
            + a2b3["coordinates"][0]
            + a2b4["coordinates"][0]
        )
        assert [30.28,-3.68] in all_the_squares_borders
        assert [30.29,-3.68] in all_the_squares_borders
        assert [30.29,-3.67] in all_the_squares_borders
        assert [30.28,-3.67] in all_the_squares_borders
        assert [30.27,-3.67] in all_the_squares_borders
        assert [30.28,-3.67] in all_the_squares_borders
        assert [30.28,-3.66] in all_the_squares_borders
        assert [30.27,-3.66] in all_the_squares_borders
        assert [30.27,-3.68] in all_the_squares_borders
        assert [30.28,-3.68] in all_the_squares_borders
        assert [30.28,-3.67] in all_the_squares_borders
        assert [30.27,-3.67] in all_the_squares_borders
        assert [30.28,-3.67] in all_the_squares_borders
        assert [30.29,-3.67] in all_the_squares_borders
        assert [30.29,-3.66] in all_the_squares_borders
        assert [30.28,-3.66] in all_the_squares_borders

    async def test_square_lock(self):
        c = CreateCommit(sid=1, type="lock", message="Foo bar bazz!!")
        await new.commit(c, self.aid1, "testuser")
        lc = None
        async with db.pool.acquire() as con:
            async with con.transaction():
                st = await con.prepare("""

                SELECT * FROM current_commits
                WHERE aid=$1 AND sid=$2
                ORDER BY cid DESC
                LIMIT 1

                """)
                r = await st.fetch(self.aid1, 1)
                lc = dict(r[0])
        assert lc["sid"] == 1
        assert lc["aid"] == self.aid1
        assert lc["author"] == "testuser"
        assert lc["message"] == "Foo bar bazz!!"
        c = CreateCommit(sid=2, type="lock")
        await new.commit(c, self.aid1, "testuser")
        lc = None
        async with db.pool.acquire() as con:
            async with con.transaction():
                st = await con.prepare("""

                SELECT * FROM current_commits
                WHERE aid=$1 AND sid=$2
                ORDER BY cid DESC
                LIMIT 1

                """)
                r = await st.fetch(self.aid1, 2)
                lc = dict(r[0])
        assert lc["sid"] == 2
        assert lc["aid"] == self.aid1
        assert lc["author"] == "testuser"
        assert lc["message"] == ""
