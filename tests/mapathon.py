"""This is load testing for the damn server."""
import gevent
import json
import jwt
import ssl
from locust import between, HttpUser, task
from pg8000 import native as db
from random import choice, uniform
from threading import Lock
from damn_server import conf

TESTED_AREAS = (998, 999)
ADD_USERS_TO_DB = False
LOAD_SCENARIO = "average"
LOAD_TIMES = {
    "average": {
        "wait": (30, 60),
        "newbie": (30, 60),
        "advanced": (30, 60),
        "reviewer": (30, 60),
    },
    "extreme": {
        "wait": (1, 2.5),
        "newbie": (5, 7.5),
        "advanced": (2.5, 5),
        "reviewer": (2.5, 5),
    },
}
MAP_TYPE = [
    "map recent", "map oldest", "map random", "map nearest",
]
REVIEW_TYPE = [
    "review recent", "review oldest", "review random", "review nearest",
    # TODO "review newbie",
]
MAPPED_TYPE = [
    "needs mapping", # 10 %
    "split", # 10 %
    "needs review", "needs review", "needs review", "needs review",
    "needs review", "needs review", "needs review", "needs review", # 80 %
]
REVIEWED_TYPE = [
    "needs mapping", "needs mapping", # 20 %
    "is done", "is done", "is done", "is done",
    "is done", "is done", "is done", "is done", # 80 %
]
uc_lock = Lock()
USERS_COUNTER = 0

class Mapper(HttpUser):
    abstract = True
    wait_time = between(*LOAD_TIMES[LOAD_SCENARIO]["wait"])

    def on_start(self):
        global USERS_COUNTER
        with uc_lock:
            self.index = USERS_COUNTER
            USERS_COUNTER += 1
        self.un = "testuser{}".format(self.index)
        self.jwt = jwt.encode(
            {"display_name": self.un},
            conf.JWT_SECRET,
            algorithm="HS256",
        )
        self.area_commits = {}
        if ADD_USERS_TO_DB:
            with db.Connection(
                user=conf.DB_USERNAME,
                host=conf.DB_HOST,
                database=conf.DB_NAME,
                password=conf.DB_PASSWORD,
            ) as con:
                con.run("""

                INSERT INTO users VALUES (:dn, '{}')
                ON CONFLICT DO NOTHING

                """, dn=self.un)

class Newbie(Mapper):
    weight = 64

    @task(1)
    def get_area(self):
        aid = choice(TESTED_AREAS)
        r = self.client.get("/area/{}".format(aid))

    @task(99)
    def map(self):
        aid = choice(TESTED_AREAS)
        self.client.headers["Content-Type"] = "application/json"
        self.client.headers["Authorization"] = "Bearer {}".format(self.jwt)
        with self.client.post("/area/{}/commits".format(aid), json={
            "type": choice(MAP_TYPE),
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
            if r.status_code == 201 and "sid" in r.json():
                sid = r.json()["sid"]
                gevent.sleep(uniform(*LOAD_TIMES[LOAD_SCENARIO]["wait"]))
                r = self.client.post("/area/{}/commits".format(
                    aid,
                ), json={
                    "sid": sid,
                    "type": choice(MAPPED_TYPE),
                    "message": "mapped by newbie",
                })

class Advanced(Mapper):
    weight = 16

    @task(60)
    def map(self):
        aid = choice(TESTED_AREAS)
        self.client.headers["Content-Type"] = "application/json"
        self.client.headers["Authorization"] = "Bearer {}".format(self.jwt)
        with self.client.post("/area/{}/commits".format(aid), json={
            "type": choice(MAP_TYPE),
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
            if r.status_code == 201 and "sid" in r.json():
                sid = r.json()["sid"]
                gevent.sleep(uniform(*LOAD_TIMES[LOAD_SCENARIO]["wait"]))
                r = self.client.post("/area/{}/commits".format(
                    aid,
                ), json={
                    "sid": sid,
                    "type": "needs review",
                    "message": "mapped by advanced",
                })

    @task(1)
    def get_stats(self):
        aid = choice(TESTED_AREAS)
        r = self.client.get("/area/{}/commits".format(aid))
        if r.status_code == 200:
            self.area_commits[aid] = list(r.json())
        r = self.client.get("/area/{}/squares".format(aid))

    @task(39)
    def merge_some_squares(self):
        aid = choice(TESTED_AREAS)
        if not aid in self.area_commits:
            return self.get_stats()
        to_merge = [
            c["sid"] for c in self.area_commits[aid]
            if c["type"] in ["to map", "to review"]
        ]
        self.client.headers["Content-Type"] = "application/json"
        self.client.headers["Authorization"] = "Bearer {}".format(self.jwt)
        with self.client.post("/area/{}/commits".format(aid), json={
            "sid": choice(to_merge),
            "type": "lock",
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
        with self.client.post("/area/{}/commits".format(aid), json={
            "sid": choice(to_merge),
            "type": "lock",
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
        with self.client.post("/area/{}/commits".format(aid), json={
            "sid": choice(to_merge),
            "type": "lock",
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
        with self.client.post("/area/{}/commits".format(aid), json={
            "sid": choice(to_merge),
            "type": "lock",
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
        with self.client.post("/area/{}/commits".format(aid), json={
            "sid": choice(to_merge),
            "type": "lock",
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
        with self.client.post("/area/{}/commits".format(aid), json={
            "sid": choice(to_merge),
            "type": "lock",
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
        r = self.client.post("/area/{}/commits".format(aid), json={
            "type": "merge",
        })

class Reviewer(Mapper):
    weight = 20

    @task(1)
    def get_stats(self):
        aid = choice(TESTED_AREAS)
        r = self.client.get("/area/{}/commits".format(aid))
        r = self.client.get("/area/{}/squares".format(aid))

    @task(99)
    def review(self):
        aid = choice(TESTED_AREAS)
        self.client.headers["Content-Type"] = "application/json"
        self.client.headers["Authorization"] = "Bearer {}".format(self.jwt)
        with self.client.post("/area/{}/commits".format(aid), json={
            "type": choice(REVIEW_TYPE),
        }, catch_response=True) as r:
            if r.status_code == 404 or r.status_code == 403:
                if "detail" in r.json():
                    r.success()
            if r.status_code == 201 and "sid" in r.json():
                sid = r.json()["sid"]
                gevent.sleep(uniform(*LOAD_TIMES[LOAD_SCENARIO]["wait"]))
                r = self.client.post("/area/{}/commits".format(
                    aid,
                ), json={
                    "sid": sid,
                    "type": choice(REVIEWED_TYPE),
                    "message": "reviewed by reviewer",
                })
