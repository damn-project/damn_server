"""Test user procedures."""
import asynctest
import datetime
import json

import create

from damn_server import db
from damn_server import area
from damn_server import list_of
from damn_server import new
from damn_server.new import CreateCommit
from damn_server import square
from damn_server import user
from damn_server.user import User

class AreaProcedures(asynctest.TestCase):
    async def setUp(self):
        await db.init_pool()
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                INSERT INTO users VALUES ('testuser', '{}')
                ON CONFLICT DO NOTHING

                """)
        self.aid1 = await area.save(create.area1(), "testuser")
        self.aid2 = await area.save(create.area2(), "testuser")
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am reviewing"),
                   (2, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (3, self.aid1, "testuser", "locked", "I am mapping"),
                   (3, self.aid1, "testuser", "to review", "Check it out"),
                   (3, self.aid1, "testuser", "locked", "I am reviewing"),
                   (3, self.aid1, "testuser", "done", "Square done"),
                ])

    async def tearDown(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                DELETE FROM current_commits;
                DELETE FROM current_squares;
                DELETE FROM current_areas;

                """)
                await con.execute("""

                DELETE FROM users WHERE display_name='testuser';

                """)
        await db.pool.close()
        db.pool = None

    async def test_unlock_locked_square(self):
        # Lock a square
        c = CreateCommit(type="map recent")
        await new.commit(c, self.aid1, "testuser")
        # First call unlocks the square
        was_locked = await new.unlock_locked_squares("testuser")
        assert was_locked == True
        # The second call does nothing
        was_locked = await new.unlock_locked_squares("testuser")
        assert was_locked == False

    async def test_user_commits(self):
        commits = await list_of.user_commits("testuser")
        since = commits[0].date
        commits = [
            {
                "sid": c.sid,
                "author": c.author,
                "type": c.type,
                "message": c.message,
            } for c in commits
        ]
        commits = tuple(commits)
        test_commits = (
            {
                "sid": 3,
                "author": "testuser",
                "type": "done",
                "message": "Square done"
            },
            {
                "sid": 3,
                "author": "testuser",
                "type": "locked",
                "message": "I am reviewing"
            },
            {
                "sid": 3,
                "author": "testuser",
                "type": "to review",
                "message": "Check it out"
            },
            {
                "sid": 3,
                "author": "testuser",
                "type": "locked",
                "message": "I am mapping"
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "to map",
                "message": "Needs more mapping"
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "locked",
                "message": "I am mapping"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "to map",
                "message": "Needs more mapping"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "locked",
                "message": "I am reviewing"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "to review",
                "message": "Check it out"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "locked",
                "message": "I am mapping"
            },
            {
                "sid": 1,
                "author": "testuser",
                "type": "to map",
                "message": "Add new square"
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "to map",
                "message": "Add new square"
            },
            {
                "sid": 3,
                "author": "testuser",
                "type": "to map",
                "message": "Add new square"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "to map",
                "message": "Add new square"
            },
            {
                "sid": 1,
                "author": "testuser",
                "type": "to map",
                "message": "Add new square"
            },
        )
        assert commits == test_commits

        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (2, self.aid1, "testuser", "to review", "Check it out"),
                   (2, self.aid1, "testuser", "locked", "I am reviewing"),
                   (2, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "to map", "Needs more mapping"),
                   (1, self.aid1, "testuser", "locked", "I am mapping"),
                   (1, self.aid1, "testuser", "to review", "Check it out"),
                   (1, self.aid1, "testuser", "locked", "I am reviewing"),
                   (1, self.aid1, "testuser", "done", "Square done"),
                ])
        commits = await list_of.user_commits("testuser", since)
        commits = [
            {
                "sid": c.sid,
                "author": c.author,
                "type": c.type,
                "message": c.message,
            } for c in commits
        ]
        commits = tuple(commits)
        test_commits = (
            {
                "sid": 1,
                "author": "testuser",
                "type": "done",
                "message": "Square done"
            },
            {
                "sid": 1,
                "author": "testuser",
                "type": "locked",
                "message": "I am reviewing"
            },
            {
                "sid": 1,
                "author": "testuser",
                "type": "to review",
                "message": "Check it out"
            },
            {
                "sid": 1,
                "author": "testuser",
                "type": "locked",
                "message": "I am mapping"
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "to map",
                "message": "Needs more mapping"
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "locked",
                "message": "I am mapping"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "to map",
                "message": "Needs more mapping"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "locked",
                "message": "I am reviewing"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "to review",
                "message": "Check it out"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "locked",
                "message": "I am mapping"
            },
        )
        assert commits == test_commits
        commits = await list_of.user_commits("testuser", since, True)
        commits = [
            {
                "sid": c.sid,
                "author": c.author,
                "type": c.type,
                "message": c.message,
            } for c in commits
        ]
        commits = tuple(commits)
        test_commits = (
            {
                "sid": 1,
                "author": "testuser",
                "type": "done",
                "message": "Square done"
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "to map",
                "message": "Needs more mapping"
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "to map",
                "message": "Needs more mapping"
            },
        )
        assert commits == test_commits

    async def test_merge_locked_squares(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.executemany("""

                INSERT INTO current_commits (sid, aid, author, type, message)
                VALUES ($1, $2, $3, $4, $5)

                """,[
                   (2, self.aid1, "testuser", "locked", "I am mapping"),
                   (4, self.aid1, "testuser", "locked", "I am mapping"),
                ])
        commits = await list_of.user_commits("testuser")
        since = commits[0].date
        c = CreateCommit(type="merge")
        await new.commit(c, self.aid1, "testuser")
        commits = await list_of.user_commits("testuser", since)
        commits = [
            {
                "sid": c.sid,
                "author": c.author,
                "type": c.type,
                "message": c.message,
            } for c in commits
        ]
        commits = tuple(commits)
        assert commits == (
            {
                "sid": 5,
                "author": "testuser",
                "type": "to map",
                "message": "Merged squares 2, 4",
            },
            {
                "sid": 4,
                "author": "testuser",
                "type": "merged",
                "message": "Merged to the square 5",
            },
            {
                "sid": 2,
                "author": "testuser",
                "type": "merged",
                "message": "Merged to the square 5",
            },
        )

class UserManagement(asynctest.TestCase):
    async def setUp(self):
        await db.init_pool()
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                DELETE FROM users WHERE display_name='testuser';

                """)

    async def tearDown(self):
        async with db.pool.acquire() as con:
            async with con.transaction():
                await con.execute("""

                DELETE FROM users WHERE display_name='testuser';

                """)
        await db.pool.close()
        db.pool = None

    async def test_add_update_user(self):
        new = User(
            display_name="testuser",
            avatar="http://some.avatar.url",
            tags="und meine tagen",
        )
        await user.save(new)
        assert new == await user.load("testuser")
        new.tags = "my tags"
        await user.update(new)
        assert new == await user.load("testuser")
        new = User(
            display_name="testuser",
            avatar="http://completelydifferent.avatar.url",
        )
        await user.update(new)
        assert new == await user.load("testuser")
