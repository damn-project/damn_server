"""Procedures related to a single area."""
import json
from datetime import datetime
from enum import Enum
from fastapi import HTTPException
from pydantic import BaseModel
from pydantic import confloat
from pydantic import conint
from pydantic import constr
from pydantic import validate_arguments
from typing import Dict

from damn_server import db

class AreaGridShape(str, Enum):
    """Shape of area grid."""
    RECTANGLE = "rectangle"
    DIAMOND = "diamond"
    FLAT_HEXAGON = "flat hexagon"
    POINTY_HEXAGON = "pointy hexagon"
    WALL_BRICK = "wall brick"
    LEFT_RHOMB = "left rhomb"
    RIGHT_RHOMB = "right rhomb"
    TOP_RHOMB = "top rhomb"
    BOTTOM_RHOMB = "bottom rhomb"

class AreaGrid(BaseModel):
    """Structure that says how the area should be divided up."""
    shape: AreaGridShape = AreaGridShape.RECTANGLE
    lat: confloat(ge=0.004) = None
    lon: confloat(ge=0.004) = None

    class Config:
        extra = "forbid"
        fields = {
            "shape": {"description": "Specify the shape of grid square."},
            "lat": {"description": "The latitude _height_ of a grid square."},
            "lon": {"description": "The longitude _width_ of a grid square."},
        }

class CreateArea(BaseModel):
    """Structure for storing information about a new area."""
    tags: str
    priority: int
    description: Dict[str, str]
    instructions: Dict[str, str]
    featurecollection: dict
    grid: AreaGrid = None

    class Config:
        extra = "forbid"
        fields = {
# I'm sorry for this formatting but I just don't know how to do it better now.
"tags": {"description": "Changeset comment #with #tags."
},
"priority": { "description":
"""The priority, actually used for sorting areas.

Set the `priority` to *less than zero* to archive the area:

- `1`: The area is used for testing. Use priority > 1 for your area.
- `0`: The area is not shown in the list of the areas.
- `-1`: The area is archived but kept.
- `-10`: The area is permanently deleted during manual upkeep.
"""
},
"description": { "description":
"""Describe the area (value) in specified language(s) (key.)"""
},
"instructions": { "description":
"""Put one world describing *what* (key) and link to wiki (value.)"""
},
"featurecollection": { "description":
"""GeoJSON boundary file.

This has to be GeoJSON FeatureCollection. If the FeatureCollection has
member *name* or the first Feature has member *properties* with member
*name*, then divide to squares function is NOT used. In such a case it's
expected you already divided the area.

You may create GeoJSON file at http://geojson.io/.
"""
},
}

@validate_arguments
async def save(
    area: CreateArea,
    author: constr(min_length=3),
) -> conint(ge=1000, le=9999):
    """Save `area` to the database, return the `area` identifier `aid`.

    Create area from `area` and save to the `current_areas` table. Also,
    create squares based on `area.featurecollection` and save them to
    `current_squares`.

    Keyword arguments
    -----------------

    - `area` -- `CreateArea` object to save.
    - `author` -- Author of new area.
    """
    ssh = False
    ssw = False
    try:
        ssh = area.grid.lat
        try:
            ssw = area.grid.lon
        except:
            ssw = ssh
    except:
        pass
    shape = False
    try:
        shape = area.grid.shape
    except:
        pass
    if (
        "type" in area.featurecollection
        and area.featurecollection["type"] == "FeatureCollection"
        and len(area.featurecollection["features"]) > 0
        and "name" in area.featurecollection
    ): # Do not divide, expect already divided area, e.g. from MapSwipe.
        shape = False
        ssh = False
        ssw = False
        q = """

        WITH tmp AS (
            INSERT INTO current_areas (
                tags,
                priority,
                description,
                instructions,
                featurecollection,
                author
            ) VALUES
            ($1, $2, $3, $4, $5, $6)
            RETURNING aid AS last_aid
        )
        INSERT INTO current_squares (sid, aid, border)
        SELECT
            row_number() OVER (),
            last_aid,
            st_square_from_already_divided(ST_GeomFromGeoJSON(f->>'geometry'))
        FROM
            tmp,
            (
                SELECT json_array_elements($5::json->'features') AS f
            ) AS fc
        RETURNING sid, aid

        """
    elif (
        "type" in area.featurecollection
        and area.featurecollection["type"] == "FeatureCollection"
        and len(area.featurecollection["features"]) > 0
    ):
        for f in area.featurecollection["features"]:
            assert f["type"] == "Feature"
            assert "geometry" in f
        q = """

        WITH tmp AS (
            INSERT INTO current_areas (
                tags,
                priority,
                description,
                instructions,
                featurecollection,
                author
            ) VALUES
            ($1, $2, $3, $4, $5, $6)
            RETURNING aid
        ),
        fc_squares AS (
            SELECT $5::json AS json
        ),
        squares AS (
            SELECT
                ST_GeomFromGeoJSON((
                    json_array_elements(json->'features')->'geometry'
                )::text) AS g
            FROM
                fc_squares
        ),
        uni_squares AS (
            SELECT
                ST_Union(g) as ug
            FROM
                squares
        )
        INSERT INTO current_squares (sid, aid, border)
        SELECT row_number() OVER () as sid, aid, sq
        FROM
            tmp,
            (
        """
        if isinstance(ssh, float) and isinstance(ssw, float):
            q += "SELECT st_divide(ug, $7, $8, $9) as sq"
        elif isinstance(shape, AreaGridShape):
            q += "SELECT st_divide(ug, $7) as sq"
        else:
            q += "SELECT st_divide(ug) as sq"
        q += """
                FROM uni_squares
            ) as foo
        RETURNING sid, aid

        """
    else:
        raise HTTPException(status_code=403, detail="bad FeatureCollection")
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare(q)
            if isinstance(ssh, float) and isinstance(ssw, float):
                r = await st.fetch(
                    area.tags,
                    area.priority,
                    json.dumps(area.description),
                    json.dumps(area.instructions),
                    json.dumps(area.featurecollection),
                    author,
                    area.grid.shape,
                    ssw,
                    ssh,
                )
            elif isinstance(shape, AreaGridShape):
                r = await st.fetch(
                    area.tags,
                    area.priority,
                    json.dumps(area.description),
                    json.dumps(area.instructions),
                    json.dumps(area.featurecollection),
                    author,
                    area.grid.shape,
                )
            else:
                r = await st.fetch(
                    area.tags,
                    area.priority,
                    json.dumps(area.description),
                    json.dumps(area.instructions),
                    json.dumps(area.featurecollection),
                    author,
                )
            aid = r[0]["aid"]
            ids = []
            for row in r:
                row = list(row)
                row.append(author)
                row.append("to map")
                row.append("Add new square")
                ids.append(tuple(row))
            await con.executemany("""

            INSERT INTO current_commits (sid, aid, author, type, message)
            VALUES ($1, $2, $3, $4, $5)

            """,
                ids
            )
            st = await con.prepare("""
            UPDATE current_areas
            SET squares_to_map=(
                SELECT max(sid)
                FROM current_squares
                WHERE aid=$1
            )
            WHERE aid=$1
            """)
            r = await st.fetch(aid)
            return aid

class UpdateArea(BaseModel):
    """Structure for storing information about an area being update.

    For more descriptive information see `CreateArea`. This class differs from
    the `CreateArea` in two things:

    1. `aid` MUST be specified.
    2. Nothing else matters. You can skip anything. (Usually you change one
       thing anyway.)

    **NOTE:** The information updated MUST be present. If not, it is untouched
    in the database. If you want to delete something, provide the *null* value
    to the property.

    **Example:** I want to delete all the `instructions` of the area ``9999``,
    so I need to ``PUT``:

        {
            aid: 9999,
            instructions: {}
        }

    When the area is updated, new commit with ``null`` square id, containing
    the current area in ``message`` is created automatically. This allows the
    *rollback* function for restoring the area from the history.
    """
    aid: conint(ge=1000, le=9999)
    tags: str = None
    priority: int = None
    description: Dict[str, str] = None
    instructions: Dict[str, str] = None

@validate_arguments
async def update(area: UpdateArea, author: constr(min_length=3)) -> None:
    """Update the `area` in the database.

    Keyword arguments
    -----------------

    - `area` -- `UpdateArea` object specifying area updated information.
    - `author` -- Author of the update.
    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare("""

            SELECT tags, priority, description, instructions
            FROM current_areas
            WHERE aid=$1

            """)
            r = await st.fetch(area.aid)
            if len(r) == 0:
                raise HTTPException(
                    status_code=404,
                    detail="area {} not in the database".format(area.aid),
                )
            old_area = dict(r[0])
            old_area["description"] = json.loads(old_area["description"])
            old_area["instructions"] = json.loads(old_area["instructions"])
            st = await con.prepare("""

            INSERT INTO current_commits (aid, author, type, message)
            VALUES ($1, $2, 'update', $3)

            """)
            r = await st.fetch(area.aid, author, json.dumps(old_area))
            for k, v in area.dict().items():
                if v is not None:
                    old_area[k] = v
            st = await con.prepare("""

            UPDATE current_areas SET
                tags=$1,
                priority=$2,
                description=$3,
                instructions=$4
            WHERE
                aid=$5

            """)
            r = await st.fetch(
                old_area["tags"],
                old_area["priority"],
                json.dumps(old_area["description"]),
                json.dumps(old_area["instructions"]),
                old_area["aid"],
            )

class Area(BaseModel):
    """Structure for storing information about an area.

    For more descriptive information see `CreateArea`. This class is
    used for retrieving information about an area from the database.
    """
    aid: conint(ge=1000, le=9999)
    tags: str
    priority: int
    description: Dict[str, str]
    instructions: Dict[str, str]
    created: datetime
    author: constr(min_length=3)
    squares_to_map: int
    squares_to_review: int
    squares_done: int

@validate_arguments
async def load(aid: conint(ge=1000, le=9999)) -> Area:
    """Return `Area` loaded from the database.

    Keyword arguments
    -----------------

    - `aid` -- `Area` identifier.
    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare("""

            SELECT
                tags,
                priority,
                description,
                instructions,
                created,
                author,
                squares_to_map,
                squares_to_review,
                squares_done
            FROM current_areas
            WHERE aid=$1

            """)
            r = await st.fetch(aid)
            if len(r) == 0:
                raise HTTPException(
                    status_code=404,
                    detail="area {} not in the database".format(aid),
                )
            a = dict(r[0])
            return Area(
                aid=aid,
                tags=a["tags"],
                priority=a["priority"],
                description=json.loads(a["description"]),
                instructions=json.loads(a["instructions"]),
                created=a["created"],
                author=a["author"],
                squares_to_map=a["squares_to_map"],
                squares_to_review=a["squares_to_review"],
                squares_done=a["squares_done"],
            )
