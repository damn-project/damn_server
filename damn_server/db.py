"""Database related procedures."""
import asyncio
import asyncpg

from damn_server import conf

pool = None

async def init_pool():
    """Initialize asyncpg pool."""
    global pool
    if pool is not None:
        return
    pool = await asyncpg.create_pool(
        "postgresql://{}:{}@{}/{}".format(
            conf.DB_USERNAME,
            conf.DB_PASSWORD,
            conf.DB_HOST,
            conf.DB_NAME,
        ),
    )
