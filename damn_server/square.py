"""Procedures related to a single square."""
import json
import xml.etree.ElementTree as ET
from fastapi import HTTPException
from pydantic import BaseModel
from pydantic import conint
from pydantic import validate_arguments

from damn_server import db

@validate_arguments
async def geojson_border(
    aid: conint(ge=1000, le=9999),
    sid: conint(ge=1),
) -> dict:
    """Return GeoJSON border geometry.

    Keyword arguments
    -----------------

    - `aid` -- `Area` identifier.
    - `sid` -- `Square` identifier.
    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare("""

            SELECT ST_AsGeoJSON(border) FROM current_squares
            WHERE aid=$1 AND sid=$2

            """)
            r = await st.fetch(aid, sid)
            if len(r) == 0:
                raise HTTPException(
                    status_code=404,
                    detail="area {} has no square {}".format(aid, sid),
                )
            return json.loads(r[0]["st_asgeojson"])

@validate_arguments
async def gpx_border(aid: conint(ge=1000, le=9999), sid: conint(ge=1)) -> str:
    """Return GPX border geometry.

    Keyword arguments
    -----------------

    - `aid` -- `Area` identifier.
    - `sid` -- `Square` identifier.
    """
    b = await geojson_border(aid, sid)
    r = ET.Element(
        "gpx",
        attrib={
            "version": "1.1",
            "creator": "Damn server",
            "xmlns": "http://www.topografix.com/GPX/1/1",
        },
    )
    trk = ET.Element("trk")
    r.append(trk)
    ET.SubElement(
        trk,
        "name",
    ).text = "Divide area {}, mapping inside square {}.".format(aid, sid)
    if b["type"] == "Polygon":
        trkseg = ET.SubElement(
            trk,
            "trkseg",
        )
        for ll in b["coordinates"][0]:
            ET.SubElement(
                trkseg,
                "trkpt",
                attrib={
                    "lon": "{}".format(ll[0]),
                    "lat": "{}".format(ll[1]),
                },
            )
    elif b["type"] == "MultiPolygon":
        for p in b["coordinates"]:
            trkseg = ET.SubElement(
                trk,
                "trkseg",
            )
            for ll in p[0]:
                ET.SubElement(
                    trkseg,
                    "trkpt",
                    attrib={
                        "lon": "{}".format(ll[0]),
                        "lat": "{}".format(ll[1]),
                    },
                )
    elif b["type"] == "LineString":
        trkseg = ET.SubElement(
            trk,
            "trkseg",
        )
        for ll in b["coordinates"]:
            ET.SubElement(
                trkseg,
                "trkpt",
                attrib={
                    "lon": "{}".format(ll[0]),
                    "lat": "{}".format(ll[1]),
                },
            )
    return ET.tostring(r, encoding="utf8")
