"""Procedures related to lists of whatever."""
import json
from datetime import datetime
from enum import Enum
from pydantic import BaseModel
from pydantic import conint
from pydantic import constr
from pydantic import validate_arguments
from typing import Tuple
from typing import Union

from damn_server import db

class ThinArea(BaseModel):
    """Lightweight area with the shortcut of the information.

    For more descriptive information see `CreateArea`. This class is
    used for retrieving shortcut of the information to list with other
    areas.
    """
    aid: conint(ge=1000, le=9999)
    tags: str
    priority: int
    created: datetime
    author: constr(min_length=3)
    squares_to_map: int
    squares_to_review: int
    squares_done: int

@validate_arguments
async def thin_areas(
    since: datetime=None,
    including_archived: bool=False,
) -> Tuple[ThinArea, ...]:
    """Return the list of shortuct information about the areas.

    Keyword arguments
    -----------------

    - `since` -- The date/time since the areas were created.
    - `including_archived` -- Include areas with the priority ``<= 0``.
    """
    q = """

    SELECT
        aid,
        tags,
        priority,
        created,
        author,
        squares_to_map,
        squares_to_review,
        squares_done
    FROM current_areas
    """
    if since and including_archived:
        q += "WHERE created>$1"
    elif since:
        q += "WHERE created>$1 AND priority>0"
    elif including_archived:
        q += ""
    else:
        q += "WHERE priority>0"
    q += """
    ORDER BY priority DESC, created ASC

    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare(q)
            if since:
                r = await st.fetch(since)
            else:
                r = await st.fetch()
            tl = []
            for a in r:
                tl.append(ThinArea(**dict(a)))
            return tuple(tl)

class AreaGeometry(BaseModel):
    """Structure for storing information about an area featurecollection."""
    aid: conint(ge=1000, le=9999)
    featurecollection: dict

@validate_arguments
async def geometries(
    including_archived: bool=False,
) -> Tuple[AreaGeometry, ...]:
    """Return the list of areas' FeatureCollections.

    Keyword arguments
    -----------------

    - `including_archived` -- Include areas with the priority ``< 0``.
    """
    q = """

    SELECT aid, featurecollection
    FROM current_areas
    """
    if including_archived:
        q += ""
    else:
        q += "WHERE priority>=0"
    q += """
    ORDER BY aid ASC

    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare(q)
            r = await st.fetch()
            gl = []
            for g in r:
                gl.append(AreaGeometry(
                    aid=g[0],
                    featurecollection=json.loads(g[1]),
                ))
            return tuple(gl)

class CommitType(str, Enum):
    """Type of area commit."""
    LOCKED = "locked"
    TO_MAP = "to map"
    TO_REVIEW = "to review"
    DONE = "done"
    SPLITTED = "splitted"
    MERGED = "merged"
    UPDATED = "update"

class Commit(BaseModel):
    """Structure for storing information about an area commit."""
    aid: conint(ge=1000, le=9999)
    sid: Union[conint(ge=1), None]
    cid: int
    date: datetime
    author: constr(min_length=3)
    type: CommitType
    message: str

    class Config:
        extra = "forbid"
        fields = {
            "aid": {"description": "Area identifier."},
            "sid": {"description": "Square identifier."},
            "cid": {"description": "Commit identifier."},
            "date": {"description": "Commit creation date/time."},
            "author": {"description": "The author of the commit."},
            "message": {"description": "Commit message."},
        }

@validate_arguments
async def area_commits(
    aid: conint(ge=1000, le=9999),
    since: datetime=None,
) -> Tuple[Commit, ...]:
    """Return the list of area commits.

    Keyword arguments
    -----------------

    - `aid` -- The area identifier.
    - `since` -- The date/time since the commits were created.
    """
    q = """

    SELECT * FROM current_commits
    WHERE aid=$1
    """
    if since:
        q += "AND date>$2"
    q += """
    ORDER BY cid DESC

    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare(q)
            if since:
                r = await st.fetch(aid, since)
            else:
                r = await st.fetch(aid)
            cl = []
            for c in r:
                cl.append(Commit(**dict(c)))
            return tuple(cl)

class Square(BaseModel):
    """Structure for storing information about a square."""
    aid: conint(ge=1000, le=9999)
    sid: conint(ge=1)
    border: dict

    class Config:
        extra = "forbid"
        fields = {
            "aid": {"description": "Area identifier."},
            "sid": {"description": "Square identifier."},
            "border": {"description": "GeoJSON of the square border."}
        }

@validate_arguments
async def area_squares(
    aid: conint(ge=1000, le=9999),
) -> Tuple[Square, ...]:
    """Return the list of area squares.

    Keyword arguments
    -----------------

    - `aid` -- The area identifier.
    """
    q = """

    SELECT aid, sid, ST_AsGeoJSON(border) FROM current_squares
    WHERE aid=$1
    ORDER BY sid ASC

    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare(q)
            r = await st.fetch(aid)
            sl = []
            for s in r:
                sl.append(Square(
                    aid=s[0],
                    sid=s[1],
                    border=json.loads(s[2]),
                ))
            return tuple(sl)

@validate_arguments
async def user_commits(
    author: constr(min_length=3),
    since: datetime=None,
    that_are_distinct: bool=False,
) -> Tuple[Commit, ...]:
    """Return the list of `author`'s commits.

    Keyword arguments
    -----------------

    - `author` -- The author of the commits to return.
    - `since` -- The date/time since the commits were created.
    - `that_are_distinct` -- Return only the last commit for each `aid-sid`.
    """
    q = "SELECT"
    if that_are_distinct:
        q += " DISTINCT ON (aid, sid)"
    q += " * FROM current_commits"
    q += " WHERE author=$1"
    if since:
        q += " AND date>$2"
    q += " ORDER BY"
    if that_are_distinct:
        q += " aid, sid,"
    q += " cid DESC"
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare(q)
            if since:
                r = await st.fetch(author, since)
            else:
                r = await st.fetch(author)
            cl = []
            for c in r:
                cl.append(Commit(**dict(c)))
            return tuple(cl)
