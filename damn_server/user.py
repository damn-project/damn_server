"""User related procedures."""
import json
from datetime import datetime
from pydantic import BaseModel
from pydantic import constr
from pydantic import validate_arguments

from damn_server import db

class User(BaseModel):
    """Structure for storing information about mapper."""
    display_name: str
    avatar: str = None
    tags: str = None
    newbie: datetime = None
    lang: str = None

    class Config:
        extra = "forbid"
        fields = {
            "display_name": {"description": "Mapper's name on OpenStreetMap."},
            "avatar": {"description": "The link to avatar."},
            "tags": {"description": "Tags added to a changeset comment."},
            "newbie": { "description": "The date till the `User` is newbie."},
            "lang": { "description": "Preferred language of the `User`."},
        }

@validate_arguments
async def save(u: User) -> None:
    """Save user `u` to the database.

    Keyword arguments
    -----------------

    - `u` -- `User` object with new user's information.
    """
    info = u.dict()
    del(info["display_name"])
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare("""

            INSERT INTO users VALUES ($1, $2)
            ON CONFLICT DO NOTHING

            """)
            r = await st.fetch(
                u.display_name,
                json.dumps(info, default=str),
            )

@validate_arguments
async def load(dn: constr(min_length=3)) -> User:
    """Load user with display name `dn` from the database.

    Keyword arguments
    -----------------

    - `dn` -- Display name of a `User`.
    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare("""

            SELECT info FROM users WHERE display_name=$1

            """)
            r = await st.fetch(dn)
            info = json.loads(dict(r[0])["info"])
            return User(
                display_name=dn,
                avatar=info["avatar"] if "avatar" in info else None,
                tags=info["tags"] if "tags" in info else None,
                newbie=info["newbie"] if "newbie" in info else None,
                lang=info["lang"] if "lang" in info else None,
            )

@validate_arguments
async def update(u: User) -> None:
    """Update user `u` in the database.

    Keyword arguments
    -----------------

    - `u` -- The `User` to update.
    """
    info = u.dict()
    del(info["display_name"])
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare("""

            UPDATE users SET info=$2
            WHERE display_name=$1

            """)
            r = await st.fetch(
                u.display_name,
                json.dumps(info, default=str),
            )
