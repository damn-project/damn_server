import os
from damn_server import conf

if "DOMAIN_NAME" in os.environ and "SERVER_SUB" in os.environ:
    conf.DAMN_SERVER = "https://{}.{}".format(
        os.environ["SERVER_SUB"],
        os.environ["DOMAIN_NAME"],
    )
    conf.DAMN_AUTH_URL = "{}/auth".format(conf.DAMN_SERVER)
    conf.OAUTH_CALLBACK_URL = "{}/authed".format(conf.DAMN_SERVER)
if "DAMN_CLIENTS" in os.environ:
    conf.DAMN_CLIENTS = os.environ["DAMN_CLIENTS"].split(",")
if "DB_HOST" in os.environ:
    conf.DB_HOST = os.environ["DB_HOST"]
if "POSTGRES_PASSWORD" in os.environ:
    conf.DB_PASSWORD = os.environ["POSTGRES_PASSWORD"]
if "POSTGRES_USER" in os.environ:
    conf.DB_USERNAME = os.environ["POSTGRES_USER"]
if "POSTGRES_DB" in os.environ:
    conf.DB_NAME = os.environ["POSTGRES_DB"]
if "JWT_SECRET" in os.environ:
    conf.JWT_SECRET = os.environ["JWT_SECRET"]
if "SESSION_SECRET" in os.environ:
    conf.SESSION_SECRET = os.environ["SESSION_SECRET"]
if "OAUTH_CONSUMER_KEY" in os.environ:
    conf.OAUTH_CONSUMER_KEY = os.environ["OAUTH_CONSUMER_KEY"]
if "OAUTH_CONSUMER_SECRET" in os.environ:
    conf.OAUTH_CONSUMER_SECRET = os.environ["OAUTH_CONSUMER_SECRET"]
