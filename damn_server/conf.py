"""Configuration constants."""
DAMN_SERVER = "http://127.0.0.1:8000"
DAMN_AUTH_URL = "{}/auth".format(DAMN_SERVER)
DAMN_CLIENTS = [
    "http://localhost",
]
DB_HOST = "127.0.0.1"
DB_NAME = "damndb"
DB_USERNAME = "damnuser"
DB_PASSWORD = "pass"
JWT_SECRET = "dd if=/dev/urandom bs=8 count=8 | base64"
SESSION_SECRET = "dd if=/dev/urandom bs=8 count=8 | base64"
OAUTH_CONSUMER_KEY = "..."
OAUTH_CONSUMER_SECRET = "..."
OAUTH_ACCESS_TOKEN_URL = "https://www.openstreetmap.org/oauth/access_token"
OAUTH_AUTHORIZE_URL = "https://www.openstreetmap.org/oauth/authorize"
OAUTH_REQUEST_TOKEN_URL = "https://www.openstreetmap.org/oauth/request_token"
OAUTH_BASE_URL = "https://api.openstreetmap.org/api/0.6/"
OAUTH_CALLBACK_URL = "{}/authed".format(DAMN_SERVER)
TMP_AUTH_STORE_DIR = "/tmp"
