"""Procedures to do some logic and then update database."""
from asyncpg.exceptions import SerializationError
from datetime import datetime
from enum import Enum
from fastapi import HTTPException
from pydantic import BaseModel
from pydantic import conint
from pydantic import constr
from pydantic import validator
from pydantic import validate_arguments
from typing import Union

from damn_server import db
from damn_server.list_of import Square
from damn_server import square

class CreateCommitType(str, Enum):
    """New commit type."""
    # Lock square commit types:
    MAP_RECENT = "map recent"
    MAP_OLDEST = "map oldest"
    MAP_RANDOM = "map random"
    MAP_NEAREST = "map nearest"
    REVIEW_RECENT = "review recent"
    REVIEW_OLDEST = "review oldest"
    REVIEW_RANDOM = "review random"
    REVIEW_NEAREST = "review nearest"
    REVIEW_NEWBIE = "review newbie"
    LOCK = "lock"
    # Unlock square commit types:
    NEEDS_MAPPING = "needs mapping"
    NEEDS_REVIEW = "needs review"
    IS_DONE = "is done"
    SPLIT = "split"
    SPLIT_HORIZONTALLY = "split horizontally"
    SPLIT_VERTICALLY = "split vertically"
    MERGE = "merge"

class CreateCommit(BaseModel):
    """Structure for storing information about a new commit to the area.

    Commit type meaning
    -------------------

    - `map recent` -- Request recent square to map.
    - `map oldest` -- Request the oldest square to map.
    - `map random` -- Request random square to map.
    - `map nearest` -- Request the nearest square to map.
    - `review recent` -- Request recent square to review.
    - `review oldest` -- Request the oldest square to review.
    - `review random` -- Request random square to review.
    - `review nearest` -- Request the nearest square to review.
    - `review newbie` -- Request newbie's square to review.
    - `lock` -- Manually lock the square. (`sid` must be set.)
    - `needs mapping` -- Mark the square to map. (`sid` must be set.)
    - `needs review` -- Mark the square to review. (`sid` must be set.)
    - `is done` -- Mark the square done. (`sid` must be set.)
    - `split` -- Split the square. (`sid` must be set.)
    - `split horizontally` -- Split horizontally. (`sid` must be set.)
    - `split vertically` -- Split vertically. (`sid` must be set.)
    - `merge` -- Merge all locked squares.
    """
    sid: Union[conint(ge=1), None] = None
    type: CreateCommitType
    message: str = ""

    @validator("type")
    def when_type_is_lock_then_sid_must_be_set(cls, t, values):
        sid_set = [
            "lock", "split", "split horizontally", "split vertically",
            "needs mapping", "needs review", "is done",
        ]
        if t in sid_set and (not "sid" in values or values["sid"] == None):
            raise ValueError("sid must be set when type in {}".format(sid_set))
        return t

    class Config:
        extra = "forbid"
        fields = {
            "sid": {"description": "Square identifier."},
            "type": {"description": "The commit type."},
            "message": {"description": "The commit message."},
        }

@validate_arguments
async def commit(
    commit: CreateCommit,
    aid: conint(ge=1000, le=9999),
    author: constr(min_length=3),
) -> Union[Square, dict]:
    """Process the `commit` and update the database.

    Keyword arguments
    -----------------

    - `commit` -- New commit to the area to process.
    - `aid` -- Area identifier.
    - `author` -- The `Author` of the commit.
    """
    if commit.type in [
        "map recent",
        "map oldest",
        "map random",
        "map nearest",
        "review recent",
        "review oldest",
        "review random",
        "review nearest",
        "review newbie",
    ]:
        s = None
        while s is None:
            try:
                s = await map_or_review_area(commit, aid, author)
            except SerializationError:
                pass
        s["border"] = await square.geojson_border(s["aid"], s["sid"])
        return Square(**s)
    elif commit.type == "lock":
        assert isinstance(commit.sid, int) and commit.sid > 0
        s = None
        while s is None:
            try:
                s = await lock_area_square(commit, aid, author)
            except SerializationError:
                pass
        s["border"] = await square.geojson_border(s["aid"], s["sid"])
        return Square(**s)
    elif commit.type == "needs mapping":
        async with db.pool.acquire() as con:
            async with con.transaction():
                st = await con.prepare("""

                SELECT * FROM current_commits
                WHERE aid=$1 AND sid=$2
                ORDER BY cid DESC
                LIMIT 1

                """)
                r = await st.fetch(aid, commit.sid)
                if r[0]["author"] != author:
                    raise HTTPException(
                        status_code=404,
                        detail="not locked by you",
                    )
                if r[0]["type"] != "locked":
                    raise HTTPException(
                        status_code=404,
                        detail="square not locked",
                    )
                st = await con.prepare("""

                INSERT INTO current_commits(sid, aid, author, type, message)
                VALUES ($1, $2, $3, 'to map', $4)

                """)
                r = await st.fetch(commit.sid, aid, author, commit.message)
                return {}
    elif commit.type == "needs review":
        async with db.pool.acquire() as con:
            async with con.transaction():
                st = await con.prepare("""

                SELECT * FROM current_commits
                WHERE aid=$1 AND sid=$2
                ORDER BY cid DESC
                LIMIT 1

                """)
                r = await st.fetch(aid, commit.sid)
                if r[0]["author"] != author:
                    raise HTTPException(
                        status_code=404,
                        detail="not locked by you",
                    )
                if r[0]["type"] != "locked":
                    raise HTTPException(
                        status_code=404,
                        detail="square not locked",
                    )
                st = await con.prepare("""

                INSERT INTO current_commits(sid, aid, author, type, message)
                VALUES ($1, $2, $3, 'to review', $4)

                """)
                r = await st.fetch(commit.sid, aid, author, commit.message)
                return {}
    elif commit.type == "is done":
        async with db.pool.acquire() as con:
            async with con.transaction():
                st = await con.prepare("""

                SELECT * FROM current_commits
                WHERE aid=$1 AND sid=$2
                ORDER BY cid DESC
                LIMIT 1

                """)
                r = await st.fetch(aid, commit.sid)
                if r[0]["author"] != author:
                    raise HTTPException(
                        status_code=404,
                        detail="not locked by you",
                    )
                if r[0]["type"] != "locked":
                    raise HTTPException(
                        status_code=404,
                        detail="square not locked",
                    )
                st = await con.prepare("""

                INSERT INTO current_commits(sid, aid, author, type, message)
                VALUES ($1, $2, $3, 'done', $4)

                """)
                r = await st.fetch(commit.sid, aid, author, commit.message)
                return {}
    elif commit.type in [
        CreateCommitType.SPLIT,
        CreateCommitType.SPLIT_HORIZONTALLY,
        CreateCommitType.SPLIT_VERTICALLY,
    ]:
        s = None
        while s is None:
            try:
                s = await split_area_square(commit, aid, author)
            except SerializationError:
                pass
        return s
    elif commit.type == "merge":
        s = None
        while s is None:
            try:
                s = await merge_area_squares(commit, aid, author)
            except SerializationError:
                pass
        s["border"] = await square.geojson_border(s["aid"], s["sid"])
        return Square(**s)
    else:
        raise HTTPException(
            status_code=404,
            detail="unknown create commit type",
        )

@validate_arguments
async def unlock_locked_squares(author: constr(min_length=3)) -> bool:
    """Unlock locked square of `author` if any.

    Return `True` if the square is unlocked or `False` if no square
    to unlock.

    Keyword arguments
    -----------------

    - `author` -- Author of the square lock commit.
    """
    async with db.pool.acquire() as con:
        async with con.transaction():
            st = await con.prepare("""

            WITH lasts AS (
                    SELECT DISTINCT ON (sid, aid) *
                    FROM current_commits
                    WHERE date + interval '2 hours' > now()
                    ORDER BY aid, sid, cid DESC
            ),
            locked AS (
                    SELECT *
                    FROM lasts
                    WHERE type='locked'
                    AND author=$1
            ),
            lasts2 AS (
                    SELECT
                        cc.sid,
                        cc.aid,
                        e.author,
                        cc.type,
                        row_number() OVER (
                            PARTITION BY cc.sid ORDER BY cc.cid DESC
                        ) as rn
                    FROM
                            locked as e,
                            current_commits as cc
                    WHERE
                            e.aid = cc.aid
                            AND e.sid = cc.sid
            )
            INSERT INTO current_commits (sid, aid, author, type, message)
            SELECT
                sid,
                aid,
                author,
                type,
                'Gonna map something else and forget to unlock'
            FROM lasts2 WHERE rn=2
            RETURNING sid, aid

            """)
            r = await st.fetch(author)
            if len(r) > 0:
                return True
            return False

@validate_arguments
async def lock_area_square(
    commit: CreateCommit,
    aid: conint(ge=1000, le=9999),
    author: constr(min_length=3),
) -> dict:
    """Request the database to lock the `commit.sid` square.

    Keyword arguments
    -----------------

    - `commit` -- New commit to the area to process.
    - `aid` -- Area identifier.
    - `author` -- The `Author` of the commit.
    """
    async with db.pool.acquire() as con:
        async with con.transaction(isolation="serializable"):
            st = await con.prepare("""

            SELECT * FROM current_commits
            WHERE aid=$1 AND sid=$2
            ORDER BY cid DESC
            LIMIT 1

            """)
            r = await st.fetch(aid, commit.sid)
            if len(r) > 0 and r[0]["type"] == "locked":
                raise HTTPException(
                    status_code=403,
                    detail="the square is already locked",
                )
            st = await con.prepare("""

            INSERT INTO current_commits(sid, aid, author, type, message)
            VALUES ($1, $2, $3, 'locked', $4)
            RETURNING sid, aid

            """)
            r = await st.fetch(commit.sid, aid, author, commit.message)
            if len(r) == 0:
                raise HTTPException(
                    status_code=404,
                    detail="no square to {}".format(commit.type),
                )
            return {"sid": r[0]["sid"], "aid": r[0]["aid"]}

@validate_arguments
async def map_or_review_area(
    commit: CreateCommit,
    aid: conint(ge=1000, le=9999),
    author: constr(min_length=3),
) -> dict:
    """Request the database for a square to map or review.

    Keyword arguments
    -----------------

    - `commit` -- New commit to the area to process.
    - `aid` -- Area identifier.
    - `author` -- The `Author` of the commit.
    """
    await unlock_locked_squares(author)
    q = ""
    if commit.type == "map recent":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (sid) cid, sid, aid, type, author
            FROM current_commits
            WHERE aid=$1
            ORDER BY sid, cid DESC
        ),
        tomap AS (
            SELECT *
            FROM last
            WHERE type='to map'
            ORDER BY cid DESC
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'I am mapping'
        FROM tomap
        RETURNING sid

        """
    elif commit.type == "map oldest":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (sid) cid, sid, aid, type, author
            FROM current_commits
            WHERE aid=$1
            ORDER BY sid, cid DESC
        ),
        tomap AS (
            SELECT *
            FROM last
            WHERE type='to map'
            ORDER BY cid ASC
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'I am mapping'
        FROM tomap
        RETURNING sid

        """
    elif commit.type == "map random":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (sid) cid, sid, aid, type, author
            FROM current_commits
            WHERE aid=$1
            ORDER BY sid, cid DESC
        ),
        tomap AS (
            SELECT *
            FROM last
            WHERE type='to map'
            ORDER BY RANDOM()
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'I am mapping'
        FROM tomap
        RETURNING sid

        """
    elif commit.type == "map nearest":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (cc.sid)
                cid, cc.sid, cc.aid, type, author, border
            FROM current_commits AS cc
            INNER JOIN current_squares AS cs
            ON (cc.aid=cs.aid AND cc.sid=cs.sid)
            WHERE cc.aid=$1
            ORDER BY cc.sid, cid DESC
        ),
        mine AS (
            SELECT border
            FROM current_commits AS cc
            INNER JOIN current_squares AS cs
            ON (cc.aid=cs.aid AND cc.sid=cs.sid)
            WHERE cc.aid=$1
            AND author=$2
            ORDER BY cid DESC
            LIMIT 1
        ),
        tomap AS (
            SELECT sid, cid, type
            FROM last, mine
            WHERE type='to map'
            ORDER BY ST_Distance(mine.border, last.border)
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'I am mapping'
        FROM tomap
        RETURNING sid

        """
    elif commit.type == "review recent":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (sid) cid, sid, type
            FROM current_commits
            WHERE aid=$1
            ORDER BY sid, cid DESC
        ),
        toreview AS (
            SELECT *
            FROM last
            WHERE type='to review'
            ORDER BY cid DESC
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'Working on review'
        FROM toreview
        RETURNING sid

        """
    elif commit.type == "review oldest":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (sid) cid, sid, type
            FROM current_commits
            WHERE aid=$1
            ORDER BY sid, cid DESC
        ),
        toreview AS (
            SELECT *
            FROM last
            WHERE type='to review'
            ORDER BY cid ASC
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'Working on review'
        FROM toreview
        RETURNING sid

        """
    elif commit.type == "review random":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (sid) cid, sid, type
            FROM current_commits
            WHERE aid=$1
            ORDER BY sid, cid DESC
        ),
        toreview AS (
            SELECT *
            FROM last
            WHERE type='to review'
            ORDER BY RANDOM()
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'Working on review'
        FROM toreview
        RETURNING sid

        """
    elif commit.type == "review nearest":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (cc.sid)
                cid, cc.sid, cc.aid, type, author, border
            FROM current_commits AS cc
            INNER JOIN current_squares AS cs
            ON (cc.aid=cs.aid AND cc.sid=cs.sid)
            WHERE cc.aid=$1
            ORDER BY cc.sid, cid DESC
        ),
        mine AS (
            SELECT border
            FROM current_commits AS cc
            INNER JOIN current_squares AS cs
            ON (cc.aid=cs.aid AND cc.sid=cs.sid)
            WHERE cc.aid=$1
            AND author=$2
            ORDER BY cid DESC
            LIMIT 1
        ),
        toreview AS (
            SELECT sid, cid, type
            FROM last, mine
            WHERE type='to review'
            ORDER BY ST_Distance(mine.border, last.border)
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'Working on review'
        FROM toreview
        RETURNING sid

        """
    elif commit.type == "review newbie":
        q += """

        WITH last AS (
            SELECT DISTINCT ON (sid) cid, sid, type, author
            FROM current_commits
            WHERE aid=$1
            ORDER BY sid, cid DESC
        ),
        toreview AS (
            SELECT sid
            FROM last
            INNER JOIN users
            ON (last.author=users.display_name)
            WHERE type='to review'
            AND (info::json->>'newbie')::timestamp > now()
            ORDER BY RANDOM()
            LIMIT 1
        )
        INSERT INTO current_commits (sid, aid, author, type, message)
        SELECT sid, $1, $2, 'locked', 'Working on review'
        FROM toreview
        RETURNING sid

        """
    else:
        raise HTTPException(
            status_code=404,
            detail="unknown create commit type",
        )
    sid = None
    async with db.pool.acquire() as con:
        async with con.transaction(isolation="serializable"):
            st = await con.prepare(q)
            r = await st.fetch(aid, author)
            if len(r) == 0:
                raise HTTPException(
                    status_code=404,
                    detail="no square to {}".format(commit.type),
                )
            sid = r[0]["sid"]
    assert isinstance(sid, int) and sid > 0
    return {"aid": aid, "sid": sid}

@validate_arguments
async def split_area_square(
    commit: CreateCommit,
    aid: conint(ge=1000, le=9999),
    author: constr(min_length=3),
) -> dict:
    """Split the square specified in `commit`.

    Keyword arguments
    -----------------

    - `commit` -- New commit to the area to process.
    - `aid` -- Area identifier.
    - `author` -- The `Author` of the commit.
    """
    cntx = 2
    cnty = 2
    if commit.type == CreateCommitType.SPLIT_HORIZONTALLY:
        cntx = 1
        cnty = 2
    elif commit.type == CreateCommitType.SPLIT_VERTICALLY:
        cntx = 2
        cnty = 1
    async with db.pool.acquire() as con:
        async with con.transaction(isolation="serializable"):
            st = await con.prepare("""

            SELECT * FROM current_commits
            WHERE aid=$1 AND sid=$2
            ORDER BY cid DESC
            LIMIT 1

            """)
            r = await st.fetch(aid, commit.sid)
            if r[0]["author"] != author:
                raise HTTPException(
                    status_code=404,
                    detail="not locked by you",
                )
            if r[0]["type"] != "locked":
                raise HTTPException(
                    status_code=404,
                    detail="square not locked",
                )
            st = await con.prepare("""

            INSERT INTO current_squares (sid, aid, border)
            SELECT max + row_number() over (), $1, sq
            FROM
                (
                    SELECT MAX(sid) as max
                    FROM current_squares
                    WHERE aid=$1
                ) as foo1,
                (
                    SELECT st_divide(border, 'rectangle',
                        $3::integer, $4::integer) as sq
                    FROM current_squares
                    WHERE aid=$1 AND sid=$2
                ) as foo2
            RETURNING sid, aid

            """)
            r = await st.fetch(aid, commit.sid, cntx, cnty)
            msg = "Sub-square of {}".format(commit.sid)
            if len(commit.message) > 0:
                msg += ": {}".format(commit.message)
            for s in r:
                st2 = await con.prepare("""

                INSERT INTO current_commits (
                    sid,
                    aid,
                    author,
                    type,
                    message
                )
                VALUES ($1, $2, $3, 'to map', $4)

                """.format(commit.sid))
                r2 = await st2.fetch(s[0], s[1], author, msg)
            st = await con.prepare("""

            INSERT INTO current_commits(sid, aid, author, type, message)
            VALUES ($1, $2, $3, 'splitted', $4)

            """)
            r = await st.fetch(commit.sid, aid, author, commit.message)
            return {}

@validate_arguments
async def merge_area_squares(
    commit: CreateCommit,
    aid: conint(ge=1000, le=9999),
    author: constr(min_length=3),
) -> dict:
    """Merge squares locked by the `author`.

    Keyword arguments
    -----------------

    - `commit` -- New commit to the area to process.
    - `aid` -- Area identifier.
    - `author` -- The `Author` of the commit.
    """
    async with db.pool.acquire() as con:
        async with con.transaction(isolation="serializable"):
            st = await con.prepare("""

            WITH lasts AS (
                SELECT DISTINCT ON (sid) *
                FROM current_commits
                WHERE aid=$1
                ORDER BY sid, cid DESC
            ),
            locked AS (
                SELECT *
                FROM lasts
                WHERE type='locked'
                AND author=$2
            ),
            to_be_merged AS (
                SELECT border
                FROM current_squares cs, locked lo
                WHERE cs.sid=lo.sid
                AND cs.aid=lo.aid
            ),
            merged AS (
                SELECT ST_UNION(border) as mb
                FROM to_be_merged
            )
            INSERT INTO current_squares (sid, aid, border)
            SELECT f.msid + row_number() OVER () as sid, f.maid as aid, mb
            FROM
                (
                    SELECT MAX(cs.sid) as msid, MAX(cs.aid) as maid
                    FROM current_squares cs, locked lo
                    WHERE cs.aid=lo.aid
                ) as f,
                merged
            RETURNING sid, aid

            """)
            r = await st.fetch(aid, author)
            if len(r) == 0:
                raise HTTPException(
                    status_code=404,
                    detail="no square locked",
                )
            assert len(r) == 1
            assert aid == r[0]["aid"]
            new_sid = r[0]["sid"]
            st = await con.prepare("""

            WITH lasts AS (
                SELECT DISTINCT ON (sid) *
                FROM current_commits
                WHERE aid=$1
                ORDER BY sid, cid DESC
            ),
            locked AS (
                SELECT *
                FROM lasts
                WHERE type='locked'
                AND author=$2
            )
            SELECT sid, aid
            FROM locked

            """)
            r = await st.fetch(aid, author)
            assert len(r) > 0
            aid = r[0]["aid"]
            ids = []
            ids_ws = []
            for row in r:
                row = list(row)
                row.append(author)
                row.append("merged")
                row.append("Merged to the square {}".format(new_sid))
                ids.append(tuple(row))
            await con.executemany("""

            INSERT INTO current_commits (sid, aid, author, type, message)
            VALUES ($1, $2, $3, $4, $5)
            """,
                ids
            )
            st = await con.prepare("""

            INSERT INTO current_commits (sid, aid, author, type, message)
            VALUES ($1, $2, $3, $4, $5)

            """)
            msg = "Merged squares "
            for row in ids:
                msg += "{}, ".format(row[0])
            r = await st.fetch(new_sid, aid, author, "to map", msg[:-2])
            return {"aid": aid, "sid": new_sid}
